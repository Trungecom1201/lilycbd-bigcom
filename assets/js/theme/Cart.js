import _ from 'lodash';
import PageManager from '../PageManager';
import utils from '@bigcommerce/stencil-utils';
import ShippingCalculator from './cart/shipping-calculator';
import CouponCodes from './cart/coupon-codes';
import GiftWrap from './cart/gift-wrap';
import GiftCertificate from './cart/gift-certificate';
import OverlayUtils from './global/overlay-utils';
import RefreshContent from './cart/refresh-content';
import ErrorAlert from './utils/error-alert';
import EditOptions from './cart/edit-options';

export default class Cart extends PageManager {
  constructor() {
    super();
    this.overlayUtils = new OverlayUtils();
    this.errorAlert = new ErrorAlert();
    this.$body = $(document.body);

    if (window.ApplePaySession && $('.dev-environment').length) {
      $(document.body).addClass('apple-pay-supported');
    }
  }

  loaded (next) {
    this.$cartContent = $('[data-cart-content]');
    this.$cartTotals = $('[data-cart-totals]');
    this.$quickCartHeader = $('[data-quick-cart-header]');
    this.$header = $('[data-cart-mobile]');
    this.$dataShowMenu = $('[data-show-menu]');

    const $els = {
      $cartContent: this.$cartContent,
      $cartTotals: this.$cartTotals,
    };


    this.shippingCalculator = new ShippingCalculator(this.context, $els);
    this.couponCodes = new CouponCodes(this.context, $els);
    this.giftCertificate = new GiftCertificate(this.context, $els);
    this.giftWrap = new GiftWrap();
    this.refreshContent = new RefreshContent($els);

    this.cartEditOptions = new EditOptions(this.context);

    this._bindEvents();
    next();
  }

  /**
   *
   * @private
   *
   * Until Cart has more than one bootstrapping method, _bindEvents() initialises all functionality
   */
  _bindEvents () {
    this.$body.on('cart-refresh', () => {

      // Unbind then rebind event listeners
      this.shippingCalculator.reInit();
      this.couponCodes.reInit();
      this.giftCertificate.reInit();
      this.giftWrap.reInit();
    });

    const debounceTimeout = 400;
    const _cartUpdate = _.bind(_.debounce(this._cartUpdate, debounceTimeout), this);
    const _cartRemoveItem = _.bind(_.debounce(this._cartRemoveItem, debounceTimeout), this);

    // cart update
    this.$cartContent.on('click', '[data-cart-update]', (event) => {
      event.preventDefault();
      const $target = $(event.currentTarget);

      if(!$target.attr('disabled')){
        // update cart quantity
        $target.attr('disabled', 'disabled');
        _cartUpdate($target);
      }
    });

    this.$cartContent.on('click', '.cart-item-remove', (event) => {
      const itemId = $(event.currentTarget).data('item-id');

      event.preventDefault();
      // remove item from cart
      _cartRemoveItem(itemId);
    });


    var currentObject = this;
    $( "body" ).delegate( ".update-qty-product-cart #update-cart-reload", "click", function() {
      currentObject.updateDataAllCart(currentObject);
    });

  }


  updateDataAllCart(currentObject){
    this.overlayUtils.show();
    window.items = [];
    $('.cart-item-quantity .form-input--incrementTotal').each(function(index){
      var itemId = $(this).attr('data-card');
      var newQty = $(this).val();

      if(index === 0){
        currentObject.updatecartData(itemId,newQty);
      }
      var data = [];
      data['id'] = itemId;
      data['qty'] = newQty;
      window.items[index] = data;
    });
  }

  updatecartData(itemId,newQty){

    var currentObject = this;
    utils.api.cart.itemUpdate(itemId, newQty, (err, response) => {
      // checking data item make sure always update all items before update again cart page
      if(window.items.length > 0){
      var i = 0;
      var index = 0;
      for(i; i < window.items.length; i++){
        if(index === 0){
          var data = window.items[i];
          if(typeof data !== "undefined"){
            currentObject.updatecartData(data['id'],data['qty']);
            window.items.splice(i,1);
            index++;
          }

        }

      }
    }else {
      // update content cart page
      if (this._refreshContent) // check if `this` refers to cart
        this._refreshContent();
    }

    // Display error message
    if (response.data.status === 'failed') {
      const element = '<input class="swal-cart-quantity-update" type="hidden" ' +
          'status="'+ response.data.status +'"' +
          'message="'+ response.data.errors.join('\n') +'" />';
      $('body').append(element);
    }

  });
  }

  //updatecartData(itemId,newQty){
  //  var self = this;
  //  utils.api.cart.itemUpdate(itemId, newQty, (err, response) => {
  //    if(response.data.status !== "failed"){
  //    self._refreshContent();
  //  }else{
  //    /*alert('ERROR: Quanitity out of stock or unavailable');*/
  //  }
  //
  //});
  //}


  _cartUpdate ($target) {
    const itemId = $target.data('item-id');
    const $el = $(`#qty-${itemId}`);
    let oldQuantity = parseInt($el.val(), 10);

    this.overlayUtils.show();
    const newQuantity = $target.data('action') === 'increase' ? oldQuantity + 1 : oldQuantity - 1;

    utils.api.cart.itemUpdate(itemId, newQuantity, (err, response) => {

      if (response && response.data.status === 'succeed') {
        // if the quantity is changed "1" from "0", we have to remove the row.
        const remove = (newQuantity === 0);
        oldQuantity = newQuantity;
        this.refreshContent.refresh('both', remove);
      } else {
        $el.val(oldQuantity);
        this.errorAlert.open(response.data.errors, 'cart-update-error');
        $target.removeAttr('disabled');
        this.overlayUtils.hide();
      }
    });
  }

  _cartRemoveItem (itemId) {
    this.overlayUtils.show();
    utils.api.cart.itemRemove(itemId, (err, response) => {
      if (response.data.status === 'succeed') {
        this._refreshContent();
      } else {
        this.errorAlert.open(response.data.errors, 'cart-remove-error');
        this.overlayUtils.hide();
      }
    });
  }

  _refreshContent() {
    const options = {
      template: {
        content: 'cart/cart-content',
        //totals: 'cart/cart-content',
        common: 'common/navigation-menu',
        dataShowMenu: 'common/navigation-menu-mobile',
        header: 'common/menu-mobile',
      },
    };

    utils.api.cart.getContent(options, (err, response) => {
    this.$cartContent.html(response.content);
    //this.$cartTotals.html(response.totals);
    this.$quickCartHeader.html(response.common);
    this.$header.html(response.header);
    this.$dataShowMenu.html(response.dataShowMenu);

    this.$cartContent.promise().done(() => {
        this.$cartTotals.promise().done(() => {
        $('[data-cart-content]', this.$cartContent).data('cart-content');
        this.overlayUtils.hide();
      });
    });

  });
  }
}
