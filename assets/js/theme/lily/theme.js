$(window).scroll(function() {
    var scrollTop = $(window).scrollTop();
    if ( scrollTop > 1 ) {
        var shipping = $('.main-header .shipping').hasClass('hide');
        if(!shipping){
            $('.main-header').addClass('header-active');
            $('header .header-container-site').addClass('active');
            $('.header-navigation-site').addClass('nav-color');
            $('header .header-logo-image.dark').addClass('active');
            $('header .header-logo-image.visible').addClass('hide-logo');
        }else {
            $('.main-header').addClass('header-active-hide');
            $('header .header-container-site').addClass('active');
            $('.header-navigation-site').addClass('nav-color');
            $('header .header-logo-image.dark').addClass('active');
            $('header .header-logo-image.visible').addClass('hide-logo');
        }
    }else {
        $('.main-header').removeClass('header-active');
        $('.main-header').removeClass('header-active-hide');
        $('.header-navigation-site').removeClass('nav-color');
        $('header .header-container-site').removeClass('active');
        $('header .header-logo-image.dark').removeClass('active');
        $('header .header-logo-image.visible').removeClass('hide-logo');
    }
});
$(document).ready(function () {
    // related product slider
    $('.products-related-container .product-grid-container').slick({
        slidesToShow:4,
        slidesToScroll:1,
        dots:false,
        nextArrow:$(".next-button"),
        prevArrow:$(".prev-button"),
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }]
    });
    // pick image for block template product
    if($('body').hasClass('template-product')){
        $(".why-cbd .right-img img").each(function () {
            var name = $(this).attr("img-name");
            var product = $('.product-area').attr("data-product-title");
            if(name === product){
                $(".why-cbd .right-img img").removeClass('active');
                $(this).addClass('active');
            }
        });
    }
    // add to cart mobile fixed
    $('.button-mobile').click(function () {
        $('.product-add-to-cart .add-to-cart').trigger('click');
        $("html, body").animate({ scrollTop: 300 }, "slow");
    });

    $('.show-popup .content-img').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2000,
    });

    //$('.carousel-wrapper').slick({
    //    dots: true,
    //    infinite: true,
    //    speed: 500,
    //    fade: true,
    //    autoplay: true,
    //    autoplaySpeed: 2000,
    //    arrows: false,
    //    cssEase: 'linear'
    //});

});


var itemMenu = $('.main-header .header-navigation-site #menu .navPages-list .navPages-item');

itemMenu.hover(function () {
    $(this).find('.navPage-subMenu').stop(true, true).slideDown(400).css('opacity','1');
    $(this).find('.navPages-action-moreIcon').addClass('active');
}, function () {
    $(this).find('.navPage-subMenu').stop(true, true).delay(200).slideUp(400).css('opacity','0');
    $(this).find('.navPages-action-moreIcon').removeClass('active');
});

$( document ).ajaxStop(function() {
    var itemMenu = $('.main-header .header-navigation-site #menu .navPages-list .navPages-item');

    itemMenu.hover(function () {
        $(this).find('.navPage-subMenu').stop(true, true).slideDown(400).css('opacity','1');
        $(this).find('.navPages-action-moreIcon').addClass('active');
    }, function () {
        $(this).find('.navPage-subMenu').stop(true, true).delay(200).slideUp(400).css('opacity','0');
        $(this).find('.navPages-action-moreIcon').removeClass('active');
    });


    var itemProduct = $('.navigation-tier-mini #quick-cart');

    itemProduct.each(function(){
        var a = $(this).find('.quick-cart-item').length;
        if(a <= 1){
            $(this).parents('.navigation-tier-mini').addClass('active');
        } else {
            $(this).parents('.navigation-tier-mini').removeClass('active');
        }
    });

});

var itemProduct = $('.navigation-tier-mini #quick-cart');

itemProduct.each(function(){
    var a = $(this).find('.quick-cart-item').length;
    if(a <= 1){
        $(this).parents('.navigation-tier-mini').addClass('active');
    } else {
        $(this).parents('.navigation-tier-mini').removeClass('active');
    }
});



$('.navigation-toggle-mobile-wrapper').click(function(){
    $('.navPages-container-mobile').addClass('active');
    $('body .mark-menu').addClass('active');
    $('body').addClass('active');
    $('.close-show-menu').addClass('active');
});

$('body .mark-menu').click(function(){
    $(this).removeClass('active');
    $('.navPages-container-mobile').removeClass('active');
    $('body').removeClass('active');
    $('.close-show-menu').removeClass('active');
});

$('.close-show-menu').click(function(){
    $(this).removeClass('active');
    $('body .mark-menu').removeClass('active');
    $('.navPages-container-mobile').removeClass('active');
    $('body').removeClass('active');
})


$('#menu-mobile .navPages-action.has-subMenu').click(function(){
    $(this).next().addClass('active');
    var a = $(this).next().height();
    var t = parseInt(a) + 51;
    $(this).parents('.navPages-list').css('height', t);
    return false;
});

$('#menu-mobile .navPages-list .navPage-subMenu .heading-subMenu').click(function(){
    $(this).parent().removeClass('active');
    $(this).parents('.navPages-list').css('height', 'auto');
});

//$(document).ready(function(){
//    var check = getCookie("fist_time");
//    if (check != 1) {
//        setCookie("fist_time", "1", 999);
//    }
//
//    function setCookie(cname, cvalue, exdays) {
//        var d = new Date();
//        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
//        var expires = "expires="+d.toUTCString();
//        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
//    }
//
//    function getCookie(cname) {
//        var name = cname + "=";
//        var ca = document.cookie.split(';');
//        for(var i = 0; i < ca.length; i++) {
//            var c = ca[i];
//            while (c.charAt(0) == ' ') {
//                c = c.substring(1);
//            }
//            if (c.indexOf(name) == 0) {
//                return c.substring(name.length, c.length);
//            }
//        }
//        return "";
//    }
//
//    var fist_time = sessionStorage.getItem("fist_time");
//    if(!fist_time){
//        sessionStorage.setItem("fist_time", "1");
//        $(".show-popup").addClass("show-banner-popup");
//        $('body').addClass('active');
//        $('.mark-menu').addClass('active');
//    }
//
//    $('.show-popup .close-popup').click(function(){
//        $(".show-popup").removeClass("show-banner-popup");
//        $('body').removeClass('active');
//        $('.mark-menu').removeClass('active');
//    });
//
//});

$(".shipping .close-shipping").click(function(){
    $('.shipping').addClass('hide');
    $('.main-content .carousel-wrapper').css('margin-top', '0px');
});

//show mini Cart
$( "body" ).delegate( ".show-mini-cart .navPages-action", "click", function() {
    $('body .navigation-wrap').addClass('active');
    $('body').addClass('active');

    var itemProduct = $('.navigation-tier-mini #quick-cart');

    itemProduct.each(function(){
        var a = $(this).find('.quick-cart-item').length;
        if(a <= 1){
            $(this).parents('.navigation-tier-mini').addClass('active');
        } else {
            $(this).parents('.navigation-tier-mini').removeClass('active');
        }
    });
});

//remove mini cart
$( "body" ).delegate( ".navigation-wrap .remove-mini-cart", "click", function() {
    $('body .navigation-wrap').removeClass('active');
    $('body').removeClass('active');
});

$( "body" ).delegate( ".navigation-wrap .modal-mask", "click", function() {
    $('body .navigation-wrap').removeClass('active');
    $('body').removeClass('active');
});


$(document).ready(function( ){
    $('.show-popup .form-submit-popup .button').click(function(){
        $('.show-popup .form-submit-popup .heading-popup-success').show();
    });
});


$( "body" ).delegate( ".product-add-to-cart .add-to-cart", "click", function() {
    $(this).addClass('progress');
    $(this).find('.button-text').text("");
});




